#include "FrameTimer.h"
using namespace std::chrono;
FrameTimer::FrameTimer()
{
	latest = steady_clock::now();
}

float FrameTimer::Mark()
{
	const auto old = latest;
	latest = steady_clock::now();
	const  duration<float>time = latest-old;
return time.count();
}
