#pragma once
#include "Colors.h"
#include "Graphics.h"


namespace SpriteEffect
{
	class Chroma {
	public:
		Chroma(Color chrom)
			:
			chroma(chrom)
		{};
		void operator()(Color cSrc, int destX, int destY, Graphics& gfx)
		{
			if (cSrc!= chroma)
			gfx.PutPixel(destX, destY, cSrc);

		}

	private:
		Color chroma;
	};

	class Substitution {
	public:
		Substitution(Color chrom,Color subst)
			:
			chroma(chrom),
			subst(subst)
		{};
		void operator()(Color cSrc, int destX, int destY, Graphics& gfx)
		{
			if (cSrc != chroma)
				gfx.PutPixel(destX, destY, subst);
		}

	private:
		Color chroma;
		Color subst;
	};
	class Def {
	public:
		
		void operator()(Color cSrc, int destX, int destY, Graphics& gfx)
		{
				gfx.PutPixel(destX, destY, cSrc);

		}		
	};
	
}
