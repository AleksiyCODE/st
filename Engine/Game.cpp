/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"
#include <iostream> 
#include <fstream>
#include <string>
#include "FrameTimer.h"
#include "SpriteEffect.h"
#include <algorithm>
Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd )
	
{
	std::wstring str;
	std::wifstream ifs;
	
	ifs.open("Data.txt", std::wifstream::in);
	
	while (ifs.good())
	{
		ifs >> str;
		if (str == L"SpriteSheetName")
		{
			ifs >> str;
			surface = Surface::FromFile(str);

		} else 
		if (str == L"NumberOfAnimationFrames")
		{
			ifs >> nFrames;
		}
		else if (str == L"NumberOfFramesInARow")
		{
			ifs >> framesInRow;
		}
		else if (str == L"NumberOfFramesInAColumn")
		{
			ifs >> framesInColumn;
		}
		else if (str == L"SingeFrameTime")
		{
			ifs >> holdTime;
		}
		

	}
	frameWidth = surface.GetWidth() / framesInRow;
	frameHeight = surface.GetHeight() / framesInColumn;
	

}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
	dt = timer.Mark();
	curFrameTimel += dt;
	while (curFrameTimel > holdTime)
	{
		curFrameTimel -= holdTime;
		
		if (++currFrame > nFrames - 1)
		{
			currFrame = 0;
		}
	}


	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		holdTime -= deltaHoldTime;
		if (holdTime<0.01f)
		{
			holdTime = 0.01f;


		}

	}
	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		holdTime += deltaHoldTime;


	}
	if (wnd.kbd.KeyIsPressed(VK_UP))
	{
		scale += 0.04;


	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		scale -= 0.04;;
		std::max(0.01f, scale);

	}
}

void Game::ComposeFrame()
{


const int sheetStartX = frameWidth * (currFrame % framesInRow);	//0.7 is a boolsit nuber that tweaks behaviour that i dont understand
const int sheetStartY = frameHeight * (currFrame / framesInRow);
const int sheetEndX = sheetStartX + frameWidth;
const int sheetEndY = sheetStartY + frameHeight;

gfx.DrawSprite(position, RectI(sheetStartY, sheetEndY, sheetStartX, sheetEndX), surface,
	SpriteEffect::Chroma(chromKey),scale);


			
}
Surface Game::surface(0, 0);
